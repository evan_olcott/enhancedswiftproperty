# EnhancedSwiftProperty
--
Swift 5.7 package that adds powerful features to your object's properties. Simply put the `@Enhanced` attribute in front of your property to enhance it, and bring more features to your Swift code.

## Observing

Observe value changes to the property.

[Code sample here]

You can silently, optionally, or forcefully notify all observers when the value is changed.
Setting the property to the current value will not notify observers.
Though explicit removal of an observer is possible, it is not required.

## Undo

Set an `UndoManager` to the property and make it available for undo/redo actions.

[Code sample here]

Observers will automatically be called on undo/redo actions.

## willSet

Set a closure to act as a precondition before a value changes.

[Code sample here]

The closure is given the current value and the proposed value.
You can return a completely different value to set to the property from this closure.

## Codable

If the property conforms to `Codable`, then so will the `@Enhanced` version.

[Code sample here]

## UserDefaults

If the property conforms to `Codable`, you can pass a `UserDefaults` key at declaration to have this value backed by `UserDefaults`.

[Code sample here]

Accepts a default value that will be used as the fallback.

//
//  EnhancedSwiftPropertyTests.swift
//
//  Created by Evan Olcott
//

@testable import EnhancedSwiftProperty
import Foundation
import XCTest

class EnhancedTests: XCTestCase
{
    @Enhanced var aProperty: [String] = ["one", "two"]
    
    var didCallObserver = false
    var expectation: XCTestExpectation?
    
    @MainActor
    func observerCalled()
    {
        XCTAssertTrue(Thread.current.isMainThread)
        didCallObserver = true
        expectation?.fulfill()
    }

    func testSimple()
    {
        $aProperty.addObserver(self) { [weak self] _ in self?.didCallObserver = true }
        aProperty = ["seven"]
        
        XCTAssertTrue(didCallObserver)
    }
    
    func testConcurrent()
    {
        expectation = XCTestExpectation(description: "")

        $aProperty.addObserver(self) { _ in Task { [weak self] in await self?.observerCalled() } }

        XCTAssertEqual(aProperty, ["one", "two"])
        aProperty = ["three", "one", "four"]
        
        wait(for: [expectation!], timeout: 2)
        XCTAssertTrue(didCallObserver)
    }
    
    func testDoNotNotify()
    {
        $aProperty.addObserver(self) { [weak self] _ in self?.didCallObserver = true }
        $aProperty.set(["seven"], notify: false)
        XCTAssertFalse(didCallObserver)
    }
    
    func testRemoveObserver()
    {
        $aProperty.addObserver(self) { [weak self] _ in self?.didCallObserver = true }
        $aProperty.removeObserver(self)
        aProperty = ["five"]
        XCTAssertFalse(didCallObserver)
    }
    
    func testWillSetHandler()
    {
        var didCallWillSetHandler = false

        $aProperty.willSetHandler =
        { current, proposed in

            didCallWillSetHandler = true
            return ["three"]
        }

        XCTAssertEqual(aProperty, ["one", "two"])
        aProperty = ["three", "one", "four"]
        
        XCTAssertTrue(didCallWillSetHandler)
        XCTAssertEqual(aProperty, ["three"])
    }

    func testUndo()
    {
        let undoManager = UndoManager()
        $aProperty.undoManager = undoManager

        XCTAssertEqual(aProperty, ["one", "two"])
        aProperty = ["three", "one", "four"]

        undoManager.undo()
        XCTAssertEqual(aProperty, ["one", "two"])
    }
}

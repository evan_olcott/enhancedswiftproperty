// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "EnhancedSwiftProperty",
    products: [
        .library(
            name: "EnhancedSwiftProperty",
            targets: ["EnhancedSwiftProperty"]),
    ],
    targets: [
        .target(
            name: "EnhancedSwiftProperty",
            dependencies: []),
        .testTarget(
            name: "EnhancedSwiftPropertyTests",
            dependencies: ["EnhancedSwiftProperty"]),
    ]
)

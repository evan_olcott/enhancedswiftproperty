//
//  EnhancedSwiftProperty.swift
//
//  Created by Evan Olcott
//

import Foundation

private extension Equatable
{
    func isEqualTo(_ rhs: Any) -> Bool
    {
        if let castRHS = rhs as? Self
        {
            return self == castRHS
        }
        else
        {
            return false
        }
    }
}

/// Details of the value change

public struct Observance<T>
{
    /// The previous value.
    public let oldValue: T

    /// The current value.
    public let newValue: T
}

@propertyWrapper
public class Enhanced<T>: Codable
{
    private var value: T
    public var projectedValue: Enhanced<T> { self }
    
    /// This value will be used for backing the value in UserDefaults
    
    var defaultsKey: String?
    
    /// This `UndoManager` will be registered with changed to this property
    /// and inherently handle undo/redo actions.

    public weak var undoManager: UndoManager?
        
    /// This handler will be called before a value is changed.
    /// The value can be changed before being returned to the caller.

    public var willSetHandler: ((_ currentValue: T, _ proposedValue: T) -> T)?
    
    enum Error: Swift.Error
    {
        case valueDoesNotConformToCodable
    }
    
    public init(wrappedValue initialValue: T, defaultsKey: String? = nil)
    {
        self.value = initialValue
        self.defaultsKey = defaultsKey
        
        if let key = defaultsKey
        {
            switch T.self
            {
            case let codableType as Codable.Type:
                if
                    let encodableValue = initialValue as? Encodable,
                    let dataValue = try? JSONEncoder().encode(encodableValue)
                {
                    UserDefaults.standard.register(defaults: [ key: dataValue ])
                }
                
                if
                    let dataValue = UserDefaults.standard.value(forKey: key) as? Data,
                    let currentValue = try? JSONDecoder().decode(codableType.self, from: dataValue) as? T
                {
                    self.value = currentValue
                }
            default:
                assertionFailure("Value must conform to Codable in order to be backed by UserDefaults")
            }
        }
    }

    public required init(from decoder: Decoder) throws
    {
        switch T.self
        {
        case let decodableType as Decodable.Type:
            let container = try decoder.singleValueContainer()
            value = try decodableType.decode(from: container) as! T
        default:
            throw Error.valueDoesNotConformToCodable
        }
    }
    
    public func encode(to encoder: Encoder) throws
    {
        if let encodableValue = value as? Encodable
        {
            var container = encoder.singleValueContainer()
            try encodableValue.encode(to: &container)
        }
        else
        {
            throw Error.valueDoesNotConformToCodable
        }
    }

    //
    // MARK: - Get/Set
    //
    
    public var wrappedValue: T
    {
        get { value }
        set { set(newValue, notify: nil) }
    }
    
    public func set(_ proposedValue: T, notify: Bool?)
    {
        observers = observers.filter { $0.observer != nil }

        let oldValue = value
        let newValue = willSetHandler.flatMap { $0(oldValue, proposedValue) } ?? proposedValue
        
        let changed: Bool
        if let oldEquatableValue = oldValue as? any Equatable,
           let newEquatableValue = newValue as? any Equatable
        {
            changed = oldEquatableValue.isEqualTo(newEquatableValue) == false
        }
        else
        {
            changed = true
        }
        
        if changed, let undoManager, undoManager.isUndoRegistrationEnabled
        {
            assert(Thread.isMainThread, "Cannot register undo to @Enhanced property in background")

            undoManager.registerUndo(withTarget: self)
            {
                $0.wrappedValue = oldValue
            }
        }

        value = newValue
        
        if
            let key = defaultsKey,
            let encodableValue = value as? Encodable,
            let dataValue = try? JSONEncoder().encode(encodableValue)
        {
            UserDefaults.standard.set(dataValue, forKey: key)
        }
        
        if notify ?? changed
        {
            let observance = Observance(oldValue: oldValue, newValue: newValue)

            observers.forEach { $0.handler(observance) }
        }
    }
    
    //
    // MARK: - Observers
    //
    
    final class ObserverWrapper
    {
        weak var observer: AnyObject?
        let handler: (Observance<T>) -> Void
        
        init(_ observer: AnyObject?, handler: @escaping @Sendable (Observance<T>) -> Void)
        {
            self.observer = observer
            self.handler = handler
        }
    }

    private var observers = [ObserverWrapper]()

    public func addObserver(_ observer: AnyObject, _ handler: @escaping @Sendable (Observance<T>) -> Void)
    {
        observers.append(ObserverWrapper(observer, handler: handler))
    }
    
    public func removeObserver(_ observer: AnyObject)
    {
        observers = observers.filter { $0.observer !== observer }
    }
}

fileprivate extension Encodable
{
    func encode(to container: inout SingleValueEncodingContainer) throws
    {
        try container.encode(self)
    }
}

fileprivate extension Decodable
{
    static func decode(from container: SingleValueDecodingContainer) throws -> Self
    {
        return try container.decode(self)
    }
}
